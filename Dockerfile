FROM archlinux

RUN pacman -Syu --noconfirm git base-devel

RUN useradd builduser -m \
 && passwd -d builduser \
 && printf 'builduser ALL=(ALL) ALL\n' | tee -a /etc/sudoers

WORKDIR /bin

RUN echo "#!/bin/bash"                                             >> build-aur \
 && echo "git clone \$1 ~/temp"                                    >> build-aur \
 && echo "cd ~/temp"                                               >> build-aur \
 && echo "sudo pacman -Sy"                                         >> build-aur \
 && echo "makepkg -s --noconfirm"                                  >> build-aur \
 && echo "mv *.tar.* /code"                                        >> build-aur \
 && echo "sudo rm -r ~/temp"                                       >> build-aur \
 && echo "echo 'INSTALL THIS PACKAGE WITH THE FOLLOWING COMMAND:'" >> build-aur \
 && echo "echo 'sudo pacman -U [package-name]'"                    >> build-aur \
 && chmod +x build-aur
    
USER builduser

WORKDIR /code
